#include <stdio.h>
#include <sqlite3.h>

#include "core/globals.h"

void sqlite_init_database(){
	printf("init sqlite db\n");
}

void sqlite_open_database_connection(){
	if(globals.sql_context.connection != NULL)
		return;

	FILE *db = fopen("test.db", "r");
	if(db == NULL)
		sqlite_init_database();
	else
		fclose(db);

	int result = sqlite3_open("test.db", (sqlite3**)&globals.sql_context.connection);
	printf("sqlite3_open result: %i\n", result);
}

void sqlite_close_database_connection(){
	if(globals.sql_context.connection == NULL)
		return;

	int result = sqlite3_close((sqlite3*)globals.sql_context.connection);
	printf("sqlite3_close result: %i\n", result);
}



void sqlite_init_sql_context(){
	globals.sql_context.open_database_connection = sqlite_open_database_connection;
	globals.sql_context.close_database_connection = sqlite_close_database_connection;
}
