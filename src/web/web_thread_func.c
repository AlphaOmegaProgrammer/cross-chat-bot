#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <unistd.h>

#include "configs/web.h"

void* web_thread_func(void *arg){
	arg = arg;

    int listener_socket = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in listener_socket_sockaddr = {0};
    listener_socket_sockaddr.sin_family = AF_INET;
    listener_socket_sockaddr.sin_port = htons(WEB_LISTEN_PORT);
	listener_socket_sockaddr.sin_addr.s_addr = inet_addr(WEB_LISTEN_IP);

	if(listener_socket_sockaddr.sin_addr.s_addr == (unsigned int)-1){
		printf("Invalid IP?\n");
		exit(0);
	}

    if(bind(listener_socket, (struct sockaddr *)&listener_socket_sockaddr, sizeof(listener_socket_sockaddr)) < 0){ 
        printf("Error binding the socket\n");
        exit(0);
    }   

    listen(listener_socket, 4);

	struct epoll_event ev;
	int epollfd = epoll_create1(0);
	if(epollfd == -1){
		perror("Epoll failed?\n");
		exit(0);
	}

	ev.events = EPOLLIN;
	ev.data.fd = listener_socket;
	if(epoll_ctl(epollfd, EPOLL_CTL_ADD, listener_socket, &ev) == -1){
		perror("epoll_ctl failed?\n");
		exit(0);
	}

	struct epoll_event event;
	int nfds;
	for(;;){
		nfds = epoll_wait(epollfd, &event, 1, -1);
		if(nfds == -1){
			perror("epoll_wait failed?\n");
			exit(0);
		}

		printf("event received\n");
	}

	return NULL;
}
