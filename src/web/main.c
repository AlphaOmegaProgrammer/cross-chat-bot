#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "web_thread_func.h"

char init_web(){
	pthread_t web_thread;

	if(pthread_create(&web_thread, NULL, web_thread_func, NULL) != 0){
		printf("Unable to make weh thread?\n");
		exit(0);
	}

	printf("thread created\n");

	if(pthread_detach(web_thread) != 0){
		printf("Unable to detach web thread?\n");
		exit(0);
	}

	return 0;
}
