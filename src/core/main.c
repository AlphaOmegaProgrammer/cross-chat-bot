#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "core/globals.h"
#include "sql/sqlite.h"
#include "web/main.h"

struct globals globals = {0};

int main(){
	globals.sql_context.error = SQL_NO_ERROR;
	globals.sql_context.connection = NULL;
	globals.sql_context.init_sql_context = sqlite_init_sql_context;

	if(globals.sql_context.init_sql_context == NULL){
		fprintf(stderr, "No database type selected?\n");
		exit(0);
	}

	globals.sql_context.init_sql_context();

	globals.sql_context.open_database_connection();
	if(globals.sql_context.error != SQL_NO_ERROR)
		exit(0);

	init_web();

	for(;;)
		sleep(100);

	globals.sql_context.close_database_connection();
	if(globals.sql_context.error != SQL_NO_ERROR)
		exit(0);

	return 0;
}
