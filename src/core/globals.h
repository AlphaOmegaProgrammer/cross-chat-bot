#ifndef __GLOBALS
#define __GLOBALS
enum sql_context_error_types {
	SQL_NO_ERROR,
	SQL_FAILED_TO_CONNECT,
	SQL_QUERY_FAILED
};

struct sql_context{
	enum sql_context_error_types error;
	void *connection;
	void (*init_sql_context)();
	void (*open_database_connection)();
	void (*close_database_connection)();
};

struct globals {
	struct sql_context sql_context;
};

extern struct globals globals;
#endif
